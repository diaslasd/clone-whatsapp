const conversasIniciais = [
  {
      "usuario": "Steve Jobs",
      "foto": "Steve_Jobs.png",
      "mensagens": [
          {
              "horario": "10:15",
              "conteudo": "Jobs, quando sai o novo Iphone?",
              "verde": true
          },
          {
              "horario": "10:15",
              "conteudo": "Em setembro de 2019",
              "verde": false
          },
      ]
  },
  {
      "usuario": "Bill Gates",
      "foto": "Bill_Gates.png",
      "mensagens": [
          {
              "horario": "10:15",
              "conteudo": "Eu queria umas dicas suas para o próximo Windows!",
              "verde": false
          },
          {
              "horario": "10:15",
              "conteudo": "Não sei se consigo... Estou meio enrolado.",
              "verde": true
          },
      ]
  },
  {
      "usuario": "Elon Musk",
      "foto": "Elon_Musk.png",
      "mensagens": [
          {
              "horario": "10:15",
              "conteudo": "O que você achou do novo recurso da AWS?!",
              "verde": false
          },
          {
              "horario": "10:15",
              "conteudo": "?!",
              "verde": false
          },
      ]
  }
];
export default conversasIniciais;