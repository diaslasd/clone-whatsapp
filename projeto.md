# Simple Clone WhatsApp <img src="src/assets/images/whatsapp.png" width="36px">

+ [youtube](https://www.youtube.com/watch?v=v_3JdYPIX5I)
+ [github](https://github.com/codigoescola/video--aprenda-vuejs-reconstruindo-interface-do-whatsapp-web/)

### Instalação 
~~~sh
vue create -b whatsclone
cd whatsclone
yarn serve  # Ctrl + C para sair
yarn add bulma
~~~

### main.js - css bulma
~~~js
import '../node_modules/bulma/css/bulma.min.css'
~~~

### index.html - icons google
~~~html
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
~~~

### App.vue - images require
~~~js
:fotoDoUsuario="require('@/assets/images/'+conversa.foto)"
~~~